## crear entorno virtual 
python -m venv myvenv
## activar entorno virtual
myvenv\Scripts\activate
## pip install by requirements
pip install -r requirements.txt

python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser

# user admin
admin
123

## Console show
Username (leave blank to use 'pc'): admin
Email address: admin@gmail.com
Password: 123
Bypass password validation and create user anyway? [y/N]: y

# run server 
python manage.py runserver



