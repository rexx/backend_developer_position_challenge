"""dacodesbackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from elearning import views
from elearning.views import login

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'courses', views.CourseViewSet)
router.register(r'lessons', views.LessonViewSet)
router.register(r'questions', views.QuestionViewSet)
router.register(r'questiontypes', views.QuestionTypeViewSet)
router.register(r'options', views.OptionViewSet)
router.register(r'students', views.StudentViewSet)
router.register(r'professors', views.ProfessorViewSet)
router.register(r'studentcourses', views.StudentCourseViewSet)
router.register(r'studentlessons', views.StudentLessonViewSet)
router.register(r'studentquestions', views.StudentQuestionViewSet)
router.register(r'studentoptions', views.StudentOptionViewSet)
router.register(r'questionnaire', views.QuestionnaireViewSet, base_name='questionnaire')


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api/login', login),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
