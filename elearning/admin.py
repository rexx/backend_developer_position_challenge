from django.contrib import admin
from .models import Course, Lesson, Question_Type, Question, Option, Student, Professor, Student_Course, Student_Lesson, Student_Question, Student_Option

# Register your models here.

admin.site.register(Course)
admin.site.register(Lesson)
admin.site.register(Question_Type)
admin.site.register(Question)
admin.site.register(Option)
admin.site.register(Student)
admin.site.register(Professor)
admin.site.register(Student_Course)
admin.site.register(Student_Lesson)
admin.site.register(Student_Question)
admin.site.register(Student_Option)