from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import views
from rest_framework.decorators import APIView
from rest_framework.response import Response
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework import authentication, permissions
from elearning.serializers import UserSerializer, GroupSerializer, QuestionnaireSerializer
from elearning.serializers import CourseSerializer
from elearning.serializers import LessonSerializer, QuestionSerializer, QuestionTypeSerializer, OptionSerializer, StudentSerializer, ProfessorSerializer, StudentCourseSerializer, StudentLessonSerializer, StudentQuestionSerializer, StudentOptionSerializer 
from .models import Course, Lesson, Question_Type, Question, Option, Student, Professor, Student_Course, Student_Lesson, Student_Question, Student_Option


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)

class QuestionnaireViewSet(viewsets.ViewSet):
    # Required for the Browsable API renderer to have a nice form.
    serializer_class = QuestionnaireSerializer

    def retrieve(self, request, pk=None):
        lesson = Lesson.objects.get(lesson_id=pk)
        questions = Question.objects.filter(question_id_lesson=lesson)
        lesson_result = None
        result = []
        for question in questions:

            optionsQuery = Option.objects.filter(option_id_lesson_question=question)
            options = []
            for option in optionsQuery:
                options.append({
                    'option_id': option.option_id,
                    'option_option': option.option_option,
                    'option_is_answer_correct': option.option_is_answer_correct,
                }) 

            result.append({
                'question_id':  question.question_id,
                'question':  question.question_question,
                'type': {
                    'id': question.question_id_question_type.question_type_id,
                    'name': question.question_id_question_type.question_type_name
                },
                'options': options
            })

        lesson_result = {
            'lesson_id': lesson.lesson_id,
            'lesson_name': lesson.lesson_name,
            'lesson_description': lesson.lesson_description,
            'lesson_approval_score': lesson.lesson_approval_score,
            'questionnaire': result
        }
        return Response(lesson_result)

    def list(self, request):
        return Response([])
    
    def create(self, request):
        data = str(request.data)
        lesson_id = request.data['lesson_id']
        questionnaires = request.data['questionnaire']
        for question in questionnaires:
            print(question['question_id'])
            print(question['type']['id'])
            print(question['options'])
            questionquery = Question.objects.get(question_id = question['question_id'])
            options = Option.objects.filter(option_id_lesson_question=questionquery)

            count_correct_anwers = 0
            for optionc in options:
                if optionc.option_is_answer_correct == 1:
                    count_correct_anwers = count_correct_anwers + 1

            #for option in options: 
            score = 0        

            for option_received in question['options']:
                optionquery = Option.objects.get(option_id = option_received['option_id'])

                for optionq in options:
                    if optionq.option_is_answer_correct == 1 and optionq.option_id == option_received['option_id']:
                        score = score + 1

                student_option = Student_Option(
                    student_option_id_option=optionquery,\
                    student_option_id_question=questionquery,\
                    student_option_id_student=self.request.user\
                    )
                student_option.save()

            passed = 0
            if question['type']['id'] == 1:
                if score == 1:
                    passed = 1
            if question['type']['id'] == 2:
                if score == 1:
                    passed = 1
            if question['type']['id'] == 3:
                if score == questionquery.question_score:
                    passed = 1
            if question['type']['id'] == 4:
                if score == count_correct_anwers:
                    passed = 1

            student_question = Student_Question(
                    student_question_passed=passed,\
                    student_question_id_question=questionquery,\
                    student_question_id_student=self.request.user\
                    )
            student_question.save()


        return Response([data])


class CourseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Course.objects.all().order_by('-course_id')
    serializer_class = CourseSerializer



    def list(self, request):
        print(self.request.user.id)
        isAdmin =  False

        for g in request.user.groups.all():
            print("grupo:")
            print(g)
            if g.id == 1:
                isAdmin = True

        queryset = Course.objects.all()
        if isAdmin!=True:
            coursesCanAccess = Student_Course.objects.all()
            #UserInstance = User.objects.get(id = self.request.user.id)
            repuesta = []

            for i in coursesCanAccess.iterator():
                print(i.student_course_id)

            for item in queryset.iterator():
                print(item.course_id)
                canAccess = False
                try:
                    coursesCanAccessObjetc = Student_Course.objects.get(student_course_id_course=item,student_course_id_student=self.request.user.id)
                    if coursesCanAccessObjetc.student_course_can_access == 1:
                        canAccess = True
                except:
                    canAccess = False
                    
                
                

                repuesta.append({
                            'course_id': item.course_id,
                            'course_name':item.course_name,
                            'course_description': item.course_description,
                            "course_order": item.course_order,
                            'can_access_student': canAccess
                        })
                #queryset[item].course_name = "changed"

            serializer = CourseSerializer(queryset, many=True)
            return Response(repuesta)
        else:
            serializer = CourseSerializer(queryset, many=True)
            return Response(serializer.data)



class LessonViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Lesson.objects.all().order_by('-lesson_id')
    serializer_class = LessonSerializer

    def list(self, request):
        print(self.request.user.id)
        isAdmin =  False

        for g in self.request.user.groups.all():
            print("grupo:")
            print(g)
            if g.id == 1:
                isAdmin = True

        queryset = Lesson.objects.all()
        if isAdmin!=True:
            coursesCanAccess = Student_Lesson.objects.all()
            #UserInstance = User.objects.get(id = self.request.user.id)
            repuesta = []

            for i in coursesCanAccess.iterator():
                print(i.student_lesson_id)

            for item in queryset.iterator():
                print(item.lesson_id)
                canAccess = False
                try:
                    coursesCanAccessObjetc = Student_Lesson.objects.get(student_lesson_id_lesson=item,student_lesson_id_student=self.request.user.id)
                    if coursesCanAccessObjetc.student_lesson_can_access == 1:
                        canAccess = True
                except:
                    canAccess = False                 

                repuesta.append({
                            'lesson_id': item.lesson_id,
                            'lesson_name':item.lesson_name,
                            'lesson_description': item.lesson_description,
                            "lesson_order": item.lesson_order,
                            'can_access_student': canAccess
                        })
                #queryset[item].course_name = "changed"

            serializer = LessonSerializer(queryset, many=True)
            return Response(repuesta)
        else:
            newqueryset = Lesson.objects.all()
            respuesta = []
            for item in newqueryset.iterator():
                respuesta.append({
                            'lesson_id': item.lesson_id,
                            'lesson_name':item.lesson_name,
                            'lesson_description': item.lesson_description,
                            "lesson_order": item.lesson_order
                        })
            return Response(respuesta)

class QuestionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Question.objects.all().order_by('-question_id')
    serializer_class = QuestionSerializer

class QuestionTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Question_Type.objects.all().order_by('-question_type_id')
    serializer_class = QuestionTypeSerializer
    
class OptionViewSet(viewsets.ModelViewSet):
    queryset = Option.objects.all().order_by('-option_id')
    serializer_class = OptionSerializer

class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all().order_by('-student_id')
    serializer_class = StudentSerializer

class ProfessorViewSet(viewsets.ModelViewSet):
    queryset = Professor.objects.all().order_by('-professor_id')
    serializer_class = ProfessorSerializer

class StudentCourseViewSet(viewsets.ModelViewSet):
    queryset = Student_Course.objects.all().order_by('-student_course_id')
    serializer_class = StudentCourseSerializer

class StudentLessonViewSet(viewsets.ModelViewSet):
    queryset = Student_Lesson.objects.all().order_by('-student_lesson_id')
    serializer_class = StudentLessonSerializer

class StudentQuestionViewSet(viewsets.ModelViewSet):
    queryset = Student_Question.objects.all().order_by('-student_question_id')
    serializer_class = StudentQuestionSerializer

class StudentOptionViewSet(viewsets.ModelViewSet):
    queryset = Student_Option.objects.all().order_by('-student_option_id')
    serializer_class = StudentOptionSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer