from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Course, Lesson, Question_Type, Question, Option, Student, Professor, Student_Course, Student_Lesson, Student_Question, Student_Option


class CourseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Course
        fields = ['course_name', 'course_description', 'course_order']

class LessonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lesson
        fields = ['lesson_name', 'lesson_description', 'lesson_order','lesson_approval_score','lesson_id_course']

class QuestionnaireSerializer(serializers.Serializer):
    lesson_id = serializers.IntegerField(read_only=True)
    lesson_name = serializers.CharField(max_length=256)
    lesson_description = serializers.CharField(max_length=256)
    lesson_approval_score = serializers.CharField(max_length=256)
    questionnaire = serializers.CharField(max_length=256)
   




class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ['question_id', 'question_question','question_score','question_id_question_type']

class QuestionTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question_Type
        fields = ['question_type_id', 'question_type_name']

class OptionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Option
        fields = ['option_id', 'option_option', 'option_is_answer_correct', 'option_id_lesson_question']
    
class StudentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student
        fields = ['student_id','student_name']

class ProfessorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Professor
        fields = ['professor_id', 'professor_name']

class StudentCourseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student_Course
        fields = ['student_course_id','student_course_can_access','student_course_passed','student_course_id_course','student_course_id_student']
    
class StudentLessonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student_Lesson
        fields = ['student_lesson_id','student_lesson_can_access','student_lesson_passed','student_lesson_score','student_lesson_id_lesson','student_lesson_id_student']

class StudentQuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student_Question
        fields = ['student_question_id','student_question_passed','student_question_id_question','student_question_id_student']

class StudentOptionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student_Option
        fields = ['student_option_id','student_option_id_option','student_option_id_question','student_option_id_student']




class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']