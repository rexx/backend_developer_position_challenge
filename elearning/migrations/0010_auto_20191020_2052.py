# Generated by Django 2.1.7 on 2019-10-21 01:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('elearning', '0009_auto_20191020_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student_question',
            name='student_question_id_student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sq_student', to=settings.AUTH_USER_MODEL),
        ),
    ]
