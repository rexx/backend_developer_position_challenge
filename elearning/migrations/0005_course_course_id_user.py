# Generated by Django 2.1.7 on 2019-10-20 18:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('elearning', '0004_auto_20191020_1252'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='course_id_user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='le_users', to=settings.AUTH_USER_MODEL),
        ),
    ]
