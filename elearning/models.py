from django.db import models
from django.contrib.auth.models import User, Group



# Create your models here.

class Course(models.Model):
    course_id = models.AutoField(primary_key=True)
    course_name = models.CharField(max_length=500, db_index=True, blank=True, null=True)
    course_description = models.CharField(max_length=1000, db_index=True, blank=True, null=True)
    course_order = models.IntegerField(default=0, null=False, blank=False)
    
    class Meta:
        verbose_name = 'course'
        verbose_name_plural = 'courses'

    def __str__(self):
        return str(self.course_name)


class Lesson(models.Model):
    lesson_id = models.AutoField(primary_key=True)
    lesson_name = models.CharField(max_length=500, db_index=True, blank=True, null=True)
    lesson_description = models.CharField(max_length=1000, db_index=True, blank=True, null=True)
    lesson_order = models.IntegerField(default=0, null=False, blank=False)
    lesson_approval_score = models.IntegerField(default=0, null=False, blank=False)
    lesson_id_course = models.ForeignKey(Course,related_name='le_courses',on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'lesson'
        verbose_name_plural = 'lessons'
           
    def __str__(self):
        return str(self.lesson_name)


class Question_Type(models.Model):
    question_type_id = models.AutoField(primary_key=True)
    question_type_name = models.CharField(max_length=500, db_index=True, blank=True, null=True)
    
    class Meta:
        verbose_name = 'question type'
        verbose_name_plural = 'question types'
           
    def __str__(self):
        return str(self.question_type_name)


class Question(models.Model):
    question_id = models.AutoField(primary_key=True)
    question_question = models.CharField(max_length=500, db_index=True, blank=True, null=True)
    question_score = models.IntegerField(default=0, null=False, blank=False)
    question_id_lesson = models.ForeignKey(Lesson,related_name='qu_lessons',db_index=True, blank=True, null=True, on_delete=models.CASCADE)
    question_id_question_type = models.ForeignKey(Question_Type,related_name='qu_question_types',on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'question'
        verbose_name_plural = 'questions'
           
    def __str__(self):
        return str(self.question_question)

class Option(models.Model):
    option_id = models.AutoField(primary_key=True)
    option_option = models.CharField(max_length=500, db_index=True, blank=True, null=True)
    option_is_answer_correct = models.IntegerField(default=0, null=False, blank=False)
    option_id_lesson_question = models.ForeignKey(Question,related_name='op_options',on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'option'
        verbose_name_plural = 'options'
           
    def __str__(self):
        return str(self.option_option)


class Student(models.Model):
    student_id = models.AutoField(primary_key=True)
    student_name = models.CharField(max_length=500, db_index=True, blank=True, null=True)

    class Meta:
        verbose_name = 'student'
        verbose_name_plural = 'students'
           
    def __str__(self):
        return str(self.student_name)


class Professor(models.Model):
    professor_id = models.AutoField(primary_key=True)
    professor_name = models.CharField(max_length=500, db_index=True, blank=True, null=True)

    class Meta:
        verbose_name = 'professor'
        verbose_name_plural = 'professors'
           
    def __str__(self):
        return str(self.professor_name)


class Student_Course(models.Model):
    student_course_id = models.AutoField(primary_key=True)
    student_course_can_access = models.IntegerField(default=0, null=False, blank=False)
    student_course_passed = models.IntegerField(default=0, null=False, blank=False)
    student_course_id_course = models.ForeignKey(Course,related_name='sc_lessons',on_delete=models.CASCADE)
    student_course_id_student = models.ForeignKey(User,related_name='cs_students',on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'student course'
        verbose_name_plural = 'student courses'
           
    def __str__(self):
        return str(self.student_course_id_course) + " ::: " + str(self.student_course_id_student)


class Student_Lesson(models.Model):
    student_lesson_id = models.AutoField(primary_key=True)
    student_lesson_can_access = models.IntegerField(default=0, null=False, blank=False)
    student_lesson_passed = models.IntegerField(default=0, null=False, blank=False)
    student_lesson_score = models.IntegerField(default=0, null=False, blank=False)
    student_lesson_id_lesson = models.ForeignKey(Lesson,related_name='sl_lesson',on_delete=models.CASCADE)
    student_lesson_id_student = models.ForeignKey(User,related_name='sl_student',on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'student lesson'
        verbose_name_plural = 'student lessons'
           
    def __str__(self):
        return str(self.student_lesson_id_lesson) + " ::: " + str(self.student_lesson_id_student)


class Student_Question(models.Model):
    student_question_id = models.AutoField(primary_key=True)
    student_question_passed = models.IntegerField(default=0, null=False, blank=False)
    student_question_id_question = models.ForeignKey(Question,related_name='sq_question',on_delete=models.CASCADE)
    student_question_id_student = models.ForeignKey(User,related_name='sq_student',on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'student question'
        verbose_name_plural = 'student questions'
           
    def __str__(self):
        return str(self.student_question_id)

class Student_Option(models.Model):
    student_option_id = models.AutoField(primary_key=True)
    student_option_id_option = models.ForeignKey(Option,related_name='sp_option',on_delete=models.CASCADE)
    student_option_id_question = models.ForeignKey(Question,related_name='sp_question',on_delete=models.CASCADE)
    student_option_id_student = models.ForeignKey(User,related_name='sp_student',on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'student option'
        verbose_name_plural = 'student options'
           
    def __str__(self):
        return str(self.student_option_id)



